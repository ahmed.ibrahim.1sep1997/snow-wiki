---
title: "How to deploy blog posts to the wiki"
date: 2023-12-28T00:23:15+02:00
draft: false
authors: ["Ejada-Team"]
# avatar: "/images/ejadaTeam.webp" 
# authors: ["Ejada-Team" , "Ahmed Waleed" ,  "Hazem M. Zaki" ,"Makarious" ,"Farha" , "Mariam" , "Rawan " , "Merna" , "Ahmed Ibrahim" ]
# description
description: "This is meta description"
image: "images/blog/02.jpg"
categories: ["General"]
draft: false
---



>  Following are the steps to follow to push posts to the wiki


* **If it's your 1st time using the repo, clone it, otherwise pull the latest repository changes to your local machine:**
 
```bash
git pull
```
* **On the repo's root directory: Add a new blog post:**
```bash
npm run blog
```

* **Replace `title` (folder+fileName) with your desired post title:**

{{< image class="img-fluid rounded-6" title="Replace 'title' (folder+fileName) with your desired post title" src="/images/change_name.png" alt="Replace 'title' (folder+fileName) with your desired post title">}}



* **Edit your post(meta-data + content):**

{{< image class="img-fluid rounded-6" title="Edit your post(meta-data + content)" src="/images/add_urPost.png" alt="Edit your post(meta-data + content)">}}



* **Add the changes to the staging area:**

```bash
git add .
```

* **Commit the changes with a descriptive message:**


```bash
git commit -m "Add new post"
```
* **Push the changes to the master branch of the remote repository:**


```bash
git push 
```

-----------------
>  Special Notes

1. `draft: true` metadata on posts makes them available only on development (Not deployed to production)  
2. Refer to `element.md` (file + `Element` page on site) for how to use markdown syntax & how it will look like
3. To run the project in development mode: Use the following command at the repo's root directory `npm run dev`


